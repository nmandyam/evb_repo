"use strict";
var application = require("tns-core-modules/application");
var appSettings = require("tns-core-modules/application-settings");
var moment = require("moment"), moment = require('moment-timezone');
var platform = require("tns-core-modules/platform");
require("nativescript-dom");

application.on("uncaughtError", function(args) {
	console.log("Uncaught system error... "); //  + JSON.stringify(args)
})

application.start({ moduleName: "pages/home/home" });

