"use strict";
var Observable = require("tns-core-modules/data/observable").Observable;
var appViewModel = new Observable();
appViewModel.selectedPage = "home";

function BasePage() {}

BasePage.prototype.viewModel = appViewModel;

appViewModel.contentLoading = function (spinState) {
}
module.exports = BasePage;
