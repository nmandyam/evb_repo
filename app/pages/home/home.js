"use strict";
var appSettings = require("tns-core-modules/application-settings"),
	BasePage = require("../../shared/BasePage"),
	fb_app = require("nativescript-plugin-firebase/app"),
	firebase = require("nativescript-plugin-firebase"),
	topmost = require('tns-core-modules/ui/frame').topmost,
	moment = require("moment"),
	platform = require("tns-core-modules/platform"),
	timerModule = require("tns-core-modules/timer");

var homeViewModel;

var HomePage = function() {
	homeViewModel = this.viewModel;
};
var cfg_json;

HomePage.prototype = new BasePage();
HomePage.prototype.constructor = HomePage;
HomePage.prototype.contentLoaded = function(args) {
}

HomePage.prototype.get_via_ui_thread = function (evt) {
	var page = topmost().currentPage;
	var status_update = page.getElementById("wah");
	var display_text = "";
	display_text += moment().format("HH:MM:ss") + " - Initialising app...\n";
	status_update.set("text", display_text);
	fb_app.initializeApp({ persist: true, projectId: 'sandbox-4dd8c' });
	display_text += moment().format("HH:MM:ss") + " - Initialized, logging in...\n";
	status_update.set("text", display_text);
	firebase.login( {
		type: firebase.LoginType.PASSWORD,
		passwordOptions: {
		  email: "nmandyam@gmail.com",
		  password: "eddyvb"
		}
	})
	.then(result => {
		var listener = fb_app.firestore().collection("eddyvb").doc("VNaFbtPF0XL7ZzvTqaBj").onSnapshot(doc => {
			if (!doc.exists) {
				//  whatever
			} else {
				var dc = doc.data();
				display_text += moment().format("HH:MM:ss") + " - App Name: " + dc.app_name + ", User: " + dc.first_name + " " + dc.last_name + "\n";
				status_update.set("text", display_text);
				display_text += moment().format("HH:MM:ss") + " - Done!\n";
				status_update.set("text", display_text);
			}
		}, function(error) {
			display_text += moment().format("HH:MM:ss") + " - Error: " + error + "\n";
			status_update.set("text", display_text);
	    });
	})
	.catch (err => {
		display_text += moment().format("HH:MM:ss") + " - Failed: " + err + "\n";
		status_update.set("text", display_text);
	});
};

HomePage.prototype.get_via_worker = function() {
	var page = topmost().currentPage;
	var display_text = moment().format("HH:MM:ss") + " - Working...\n";
	var status_update = page.getElementById("wah");
	status_update.set("text", display_text);
	var worker = new Worker("~/fStore_worker");
	worker.onmessage = function(msg) {
		var response = msg.data;
		if (msg.data.type == 'message') {
			display_text += moment().format("HH:MM:ss") + " - Worker says: " + msg.data.value + "\n";
			status_update.set("text", display_text);
		} else if (msg.data.type == 'doc') {
			var resp = msg.data.doc;
			display_text += moment().format("HH:MM:ss") + " - App Name: " + resp.app_name + ", User: " + resp.first_name + " " + resp.last_name + "\n";
			status_update.set("text", display_text);
			display_text += moment().format("HH:MM:ss") + " - Done!\n";
			status_update.set("text", display_text);
		}
	}
    worker.onerror = function(err) {
        console.log('An unhandled error occurred in worker: ' + err.filename + ', line: ' + err.lineno);
        console.log(err.message);
    }
	worker.postMessage("abc");
};


module.exports = new HomePage();
